package com.sistemaCartoes.cartoes.cartao.repository;

import com.sistemaCartoes.cartoes.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numero);
}
