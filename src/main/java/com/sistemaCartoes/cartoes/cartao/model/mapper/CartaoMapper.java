package com.sistemaCartoes.cartoes.cartao.model.mapper;

import com.sistemaCartoes.cartoes.cartao.model.Cartao;
import com.sistemaCartoes.cartoes.cartao.model.dto.create.CreateCartaoRequest;
import com.sistemaCartoes.cartoes.cartao.model.dto.create.CreateCartaoResponse;
import com.sistemaCartoes.cartoes.cartao.model.dto.get.GetCartaoResponse;
import com.sistemaCartoes.cartoes.cartao.model.dto.update.UpdateCartaoRequest;
import com.sistemaCartoes.cartoes.cartao.model.dto.update.UpdateCartaoResponse;
import com.sistemaCartoes.cartoes.cliente.model.Cliente;

public class CartaoMapper {

    public static Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        Cliente client = new Cliente();
        client.setId(cartaoCreateRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());

        cartao.setClient(client);
        return cartao;
    }

    public static CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getClient().getId());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClient().getId());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClient().getId());

        return getCartaoResponse;
    }

}
