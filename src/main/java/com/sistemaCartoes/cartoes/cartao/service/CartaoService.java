package com.sistemaCartoes.cartoes.cartao.service;

import com.sistemaCartoes.cartoes.cartao.excepetion.CartaoAlreadyExistsException;
import com.sistemaCartoes.cartoes.cartao.excepetion.CartaoNotFoundException;
import com.sistemaCartoes.cartoes.cartao.model.Cartao;
import com.sistemaCartoes.cartoes.cliente.model.Cliente;
import com.sistemaCartoes.cartoes.cartao.repository.CartaoRepository;
import com.sistemaCartoes.cartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clientService;

    public Cartao create(Cartao cartao) {
        // Bloco de validação
        Cliente client = clientService.getById(cartao.getClient().getId());
        cartao.setClient(client);

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }

        // Regras de negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}
