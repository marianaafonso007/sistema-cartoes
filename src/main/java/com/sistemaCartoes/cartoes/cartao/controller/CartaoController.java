package com.sistemaCartoes.cartoes.cartao.controller;

import com.sistemaCartoes.cartoes.cartao.model.Cartao;
import com.sistemaCartoes.cartoes.cartao.model.dto.create.CreateCartaoRequest;
import com.sistemaCartoes.cartoes.cartao.model.dto.create.CreateCartaoResponse;
import com.sistemaCartoes.cartoes.cartao.model.dto.get.GetCartaoResponse;
import com.sistemaCartoes.cartoes.cartao.model.dto.update.UpdateCartaoRequest;
import com.sistemaCartoes.cartoes.cartao.model.dto.update.UpdateCartaoResponse;
import com.sistemaCartoes.cartoes.cartao.model.mapper.CartaoMapper;
import com.sistemaCartoes.cartoes.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {

        Cartao cartao = CartaoMapper.fromCreateRequest(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return CartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        Cartao cartao = CartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.update(cartao);
        return CartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);

        return CartaoMapper.toGetResponse(byNumero);
    }
}
