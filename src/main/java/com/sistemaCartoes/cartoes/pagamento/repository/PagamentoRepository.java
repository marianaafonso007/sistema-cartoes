package com.sistemaCartoes.cartoes.pagamento.repository;

import com.sistemaCartoes.cartoes.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findAllByCartao_id(Long cartao_id);
}
