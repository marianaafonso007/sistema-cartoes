package com.sistemaCartoes.cartoes.pagamento.service;

import com.sistemaCartoes.cartoes.cartao.model.Cartao;
import com.sistemaCartoes.cartoes.cartao.service.CartaoService;
import com.sistemaCartoes.cartoes.pagamento.model.Pagamento;
import com.sistemaCartoes.cartoes.pagamento.repository.PagamentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private CartaoService cartaoService;

    public Pagamento criarPagamento(Pagamento pagamento){
        Cartao cartao = cartaoService.getById(pagamento.getCartao().getId());
        if(cartao.getAtivo() == true){
            pagamento.setCartao(cartao);
            return pagamentoRepository.save(pagamento);
        }
        throw new ObjectNotFoundException("", "Cartão não está ativo para pagamentos.");
    }

    public List<Pagamento> buscarPagamentoPorIdCartao(Long cartao_id){
        return pagamentoRepository.findAllByCartao_id(cartao_id);
    }
}
