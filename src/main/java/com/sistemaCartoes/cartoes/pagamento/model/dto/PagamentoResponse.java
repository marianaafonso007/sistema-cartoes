package com.sistemaCartoes.cartoes.pagamento.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PagamentoResponse {

    private Long id;

    private String descricao;

    private Double valor;

    @JsonProperty("cartao_id")
    private Long cartaoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }
}