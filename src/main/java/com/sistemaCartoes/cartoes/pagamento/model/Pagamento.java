package com.sistemaCartoes.cartoes.pagamento.model;

import com.sistemaCartoes.cartoes.cartao.model.Cartao;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Para realizar a compra é necessário informar um cartão válido.")
    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Cartao cartao;
    @NotNull(message = "Para seguir com a compra favor preencher a descrição.")
    private String descricao;
    @NotNull(message = "Necessário informar o valor da compra.")
    private Double valor;

    public Pagamento(@NotNull(message = "Para realizar a compra é necessário informar um cartão válido.") Cartao cartao, @NotNull(message = "Para seguir com a compra favor preencher a descrição.") String descricao, @NotNull(message = "Necessário informar o valor da compra.") Double valor) {
        this.cartao = cartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Pagamento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
