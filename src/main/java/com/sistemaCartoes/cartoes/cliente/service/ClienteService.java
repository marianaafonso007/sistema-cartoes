package com.sistemaCartoes.cartoes.cliente.service;

import com.sistemaCartoes.cartoes.cliente.exception.ClientNotFoundException;
import com.sistemaCartoes.cartoes.cliente.model.Cliente;
import com.sistemaCartoes.cartoes.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id) {
        Optional<Cliente> byId = clienteRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClientNotFoundException();
        }

        return byId.get();
    }
}
