package com.sistemaCartoes.cartoes.cliente.repository;

import com.sistemaCartoes.cartoes.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

}
